#include "Binary Tree.h"
#include "BTNode.h"

BinaryTree::BinaryTree() {
	root = NULL;
}

BinaryTree::BinaryTree(Node* r) {
	root = r;
}

BinaryTree::BinaryTree(int d, const BinaryTree& bLeft = BinaryTree(), const BinaryTree& bRight = BinaryTree()) {
	// Initialize root node with new data, and set left and right ptrs and left and right subtrees, which are initially empty
	root = new Node(d, bLeft.root, bRight.root);
}

BinaryTree::~BinaryTree() {}

ostream& operator<< (ostream& w, const BinaryTree bt) {
	w << bt.root->data;
	return w;
}



bool BinaryTree::IsLeaf() {
	if (root != NULL)
		return (root->left == NULL && root->right == NULL); // both left and right ptrs must point to NULL for tree to be leaf
	else
		return true;
}

bool BinaryTree::fullBTCheck(Node *r) {
	// If tree is empty, binary tree is full
	if (r == NULL) {
		return true;
	}

	// If tree is leaf, binary tree is full
	if (r->left == NULL && r->right == NULL) {
		return true;
	}

	// If both left and right ptrs are not NULL, recursively call function on left and right subtrees
	if ((r->left) && (r->right))
		return (fullBTCheck(r->left) && fullBTCheck(r->right));

	else
		return false;
}

bool BinaryTree::fullBTCheck() {
	return fullBTCheck(root);
}
