// Write  2  C++  methods  to  compute  the  dot
//product  of  two  vectors. The  first  method  will  use  a  regular  iteration  process  to
//complete  the  calculation, the  second  method  will  use  the  parallelism  concept  using
//openMP.

#include <iostream>
#include <vector>
#include <omp.h>

using namespace std;

int CalcDotProduct(vector<int> Vector1, vector<int> Vector2) {

	// Create variable to store dot product and initialize it to 0
	int dp = 0;

	int size = Vector1.size();

	for (int i = 0; i < size; i++) {
		dp = dp + (Vector1[i] * Vector2[i]);
	}

	return dp;
}

int ParallelDotProduct(vector<int> Vector1, vector<int> Vector2) {

	// Create variable to store dot product and initialize it to 0
	int dp2 = 0;

	int vecSize = Vector1.size();
	
	omp_set_num_threads(4); // Create 4 threads
	
	// Create temp variable to store product 
	int tempProduct = 0;

# pragma omp parallel for num_threads(4)
		for (int i = 0; i < vecSize; i++) 
		{
		tempProduct = Vector1[i] * Vector2[i];

# pragma omp atomic // Atomic operations to solve race condition
		dp2 += tempProduct;
		}

	return dp2;
}

int main()
{
	// Create and populate 2 vectors
	vector<int> vec1{ 1, 2, 3, 4 };
	vector<int> vec2{ 2, 2, 0, 5};

	// Use standard iteration process to calculate dot product of 2 vectors
	cout << "The result using the standard iteration process: " << CalcDotProduct(vec1, vec2) << endl;

	// Use parallelism to calculate dot product of 2 vectors
	cout << "The result using parallelism: " << ParallelDotProduct(vec1, vec2) << endl;

	return 0;
}