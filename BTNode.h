#pragma once

#ifndef NODE_H
#define NODE_H

#include <iostream>
using namespace std;

struct Node {
	int data;
	//Left ptr
	Node* left;
	// Right ptr
	Node* right;

	// Node Constructor
	Node(int D, Node* l, Node* r);

	// Alternate Node Constructor
	Node(int D);

	// Node Destructor; recommended to keep as virtual for optimization purposes
	virtual ~Node();

	friend ostream& operator << (ostream& w, const Node& t);
};

#endif