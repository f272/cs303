#pragma once

#ifndef BINARYTREE_H
#define BINARYTREE_H

#include "BTNode.h"

class BinaryTree {
protected:
	Node* root;

public:
	// 3 types of constructors
	// 1) Initialize empty tree
	BinaryTree();
	// 2) Passing data in for root node
	BinaryTree(Node* r);
	// 3) Passing data for root, and passing 2 binary trees to the left and right
	BinaryTree(int d, const BinaryTree& bleft, const BinaryTree& bRight);
	virtual ~BinaryTree();
	friend ostream& operator  << (ostream& w, const BinaryTree bt);

	// Check if tree is empty
	bool IsNull();

	// Check if tree is a leaf
	bool IsLeaf();

	// Check if tree is a full binary tree
	bool fullBTCheck(Node* r);

	// Check if tree is a full binary tree
	bool fullBTCheck();
	

};

#endif