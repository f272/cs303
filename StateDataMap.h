#pragma once

#include <iostream> 
#include <iterator> 
#include <map> 
#include "myMap.h"

using namespace std;

class stateDataMap {
public:
	stateDataMap(map<string, string> data) {
		container.insert(data.begin(),data.end());
	}
	string& operator[](string c) {
		map<string, string>::iterator itsub = container.find(c);
		return itsub->second;
	}

private:
	map<string, string> container;
};
