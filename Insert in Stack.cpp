#include<iostream>
#include<list>
#include<iterator>
#include<stack>
using namespace std;

	// Function to print contents of stack
	void printStack(stack<int> Stack) {
		// Create a temporary stack
		stack<int> temp2;
		int size = Stack.size();
		for (int i = 0; i < size; i++) {
			// Use a for loop to read the top of the stack and then pushing it into a second stack, then removing it from the original stack
			temp2.push(Stack.top());
			cout << Stack.top() << endl;
			Stack.pop();
		}
	}

	// Function to insert element at nth position from top in stack
	void insertInStack(stack<int> Stack, int index, int data)
	{
		// Create a temporary stack
		stack<int> temp;

		cout << "The size of the original stack is: " << Stack.size() << endl; 
		int size = Stack.size();

		// Iterate over all elements of original stack
		for (int i = 0; i < size + 1; i++) {

			// Check if counter reached desired index where we want to insert the new element
			if (i == index - 1) {
				temp.push(data);
				continue;
			}

			// Otherwise, use a for loop to read the top of the stack and then pushing it into the temp stack, then removing it from the original stack
			temp.push(Stack.top());
			Stack.pop();
		}
	
		cout << "The contents of the temp stack at this point are: " << endl;
		printStack(temp);
		cout << "The size of the temp stack is: " << temp.size() << endl;

		// Then push the elements of the temp stack back into the original stack while clearing elements from the temp array
		
		int tempsize = temp.size();
		for (int i = 0; i < tempsize; i++) {
			Stack.push(temp.top());
			cout << "Element inserted: "<< Stack.top() << endl;
			temp.pop();
		}
		// Display contents of stack with inserted item
		cout << "The contents of this stack after running the insert function are: " << endl;
			printStack(Stack);

	}

int main()
{
	// Create a Stack vector and populate it with numbers in the following order: 4, 3, 2, 1
	stack<int> myStack;
	myStack.push(4);
	myStack.push(3);
	myStack.push(2);
	myStack.push(1);

	// Display current contents of stack
	printStack(myStack);

	// Run the insert in stack function to insert the number 7 at position 3
	insertInStack(myStack, 3, 7);

	return 0;
}
