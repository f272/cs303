//Trace the execution of the call mystery(10) for the following
//recursive function.

#include<iostream>
#include<list>
#include<stack>
using namespace std;

void printStackBottom(stack<int> Stack) {
	// Create a temporary stack
	stack<int> temp2;
	int size = Stack.size();
	for (int i = 0; i < size - 1; i++) {
		// Use a for loop to remove all but the bottom element of the original stack
		temp2.push(Stack.top());
		Stack.pop();
	}

	cout << Stack.top() << endl;

	int tempsize = temp2.size();

	// Push elements from the temp stack back into the original stack while clearing temp stack
	for (int j = 0; j < tempsize; j++) {
		Stack.push(temp2.top());
		temp2.pop();
	}
}

int UMKC(int n) {
	// Create a temporary stack
	stack<int> temp;
	if (n == 0) {
		return 0;
	}
	else {
		int result = n * n + UMKC(n - 2);
		cout << "n= " << n << endl;
		temp.push(result);
		cout << "Result pushed to stack: " << endl;
		printStackBottom(temp);
		cout << endl;

		return result;
	}
}

int main() {
	UMKC(10);
	return 0;
}