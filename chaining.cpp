#include<list>
#include<iostream>
#include<iterator>
#include<utility>
#include<string>
#include<vector>
#include<cmath>
using namespace std;

//Use chaining method CPP files provided in Module 11 to complete the following :
//
//1 - Write a function to add a pair element to the hash table.

void AddPair(pair<string, char> pair, list<char>* hashTable, int hashSize) {
	cout << "AddPair called" << endl;
	// Calculate hash index using pair's key
	int hashIndex = 0;
	for (int x = 0; x < 3; x++) {
		// Calculate hash value for key (first value) of pair given its index
		hashIndex += ((int)((pair.first)[x]) * (int)pow(31, x));
	}

	// Modulate hash index with the indicated hash size
	hashIndex %= hashSize;

	//// Search the hash table at the indicated index for the value
	std::list<char>::iterator begin = hashTable[hashIndex].begin();
	std::list<char>::iterator end = hashTable[hashIndex].end();
	std::list<char>::iterator it = find(begin, end, pair.second);
	
		// If iterator is not pointing at the end of the list, it means the value already exists in the list
		if (it != end) {
			// Don't insert the pair into the linked list since value's already there
		}
	
		else {
			// Otherwise, value is not there; add the pair to the end of the linked list
			hashTable[hashIndex].push_back(pair.second);
		}
}

//2 - Write a function to remove an element to the hash table.

void RemovePair(pair<string, char> pair, list<char>* hashTable, int hashSize) {
	cout << "RemovePair called" << endl;
	// Calculate hash index using pair's key
	int hashIndex = 0;
	for (int x = 0; x < 3; x++) {
		// Calculate hash value for key (first value) of pair given its index
		hashIndex += ((int)((pair.first)[x]) * (int)pow(31, x));
	}

	// Modulate hash index with the indicated hash size
	hashIndex %= hashSize;

	//// Search the hash table at the indicated index for the value
	std::list<char>::iterator it = find(hashTable[hashIndex].begin(), hashTable[hashIndex].end(), pair.second);

	// If iterator is not pointing at the end of the list, it means the value exists in the list
	if (it != hashTable[hashIndex].end()) {
		// Remove element from the linked list
		hashTable[hashIndex].remove(pair.second);
	}

	else {
		// Otherwise, key is not there
		cout << "Value not found!" << endl;
	}
}

//3 - Write a method to traverse the hash table.

// Loops through hash table and prints out linked list at every index 
void TraverseTable(list<char>* hashTable, int hashSize) {
	for (int i = 0; i < hashSize; i++) {
		cout << i << ": ";
		if (hashTable[i].empty()) {
			cout << "NULL" << endl;
			continue;
		}
		else if (hashTable[i].front() != NULL) {
			list<char>::iterator b = hashTable[i].begin();
			list<char>::iterator e = hashTable[i].end();
			for (; b != e; b++) {
				cout << *b << "->";
			}
			cout << "NULL" << endl;
		}
	}
}
//4 - Write a method to rehash the hash table using a size passed as argument, the size should be greater than the original hash table size.
std::list<char>* RehashTable(list<char>* hashTable, int hashSize, int newhashSize) {
	// Allocate space for a new hash table with given hash size
	list<char>* hashTable2 = new list<char>[newhashSize];

	for (int i = 0; i < hashSize; i++) {
		if (hashTable[i].empty()) {
			continue;
		}
		else if (hashTable[i].front() != NULL) { // As long as linked list at that index is not empty, insert the linked list from original hash table to new hash table                    
			// Creat a temp iterator
			list<char>::iterator iter = hashTable[i].begin();
				while (iter != hashTable[i].end()) {
					hashTable2[i].push_back(*iter);
					iter++;
				}
		}
	}

	// Free up space taken up by original Hash Table
	delete[] hashTable;
	return hashTable2;
}


int main() {
	// Create a vector of pairs, with string 1st value and char 2nd value
	pair<string, char> orig[] = {pair<string,char>("r98",'A'),
									pair<string,char>("11y",'L'),
									pair<string,char>("q54",'G'),
									pair<string,char>("p88",'O'),
									pair<string,char>("bb1",'R'),
									pair<string,char>("www",'I'),
									pair<string,char>("nbn",'T'),
									pair<string,char>("pop",'H'),
									pair<string,char>("87u",'M'),
									pair<string,char>("ede",'S'),
									pair<string,char>("kk8",'A'),
									pair<string,char>("711",'L'),
									pair<string,char>("mom",'G'),
									pair<string,char>("28m",'O')
									};

	
	int dataSize = *(&orig + 1) - orig;
	int hashSize = 20;

	// Hash table is a list of chars; allocates space for a new list with the given hash table size of 20
	list<char>* hashTable= new list<char>[hashSize];

	// Creates variable for hash table index
	int index;
	// Uses a for loop to calculate the hash index, then push back char variable into list at given index
	for (int i = 0; i < dataSize; i++) {
		index = 0; // Assign index of 0 for each element at the beginning 
		for (int x = 0; x < 3; x++) {
			// Calculate Hash function value for each key (first value)
			index += ((int)((orig[i].first)[x])*(int)pow(31, x));	//((int)orig[i]* 31) % 11; Multiple each key by 31^x where x is the index
		}
		// Modulate hash key with the indicated Hashsize
		index %= hashSize;

		// Push value of original array (2nd value in pair) into hash table at modulated index
		hashTable[index].push_back(orig[i].second);
	}

	TraverseTable(hashTable, hashSize);
	
	// Add a value to the hash table
	cout << endl;
	AddPair(pair<string, char>("799", 'X'), hashTable, hashSize);
	cout << endl;

	cout << "Traversal of table after addition: " << endl;
	TraverseTable(hashTable, hashSize);

	// Remove that pair from the linked list
	cout << endl;
	RemovePair(pair<string, char>("799", 'X'), hashTable, hashSize);
	cout << endl;
	
	cout << "Traversal of table after removal: " << endl;
	TraverseTable(hashTable, hashSize);

	// Rehash table with size of 25 and store as new hash table
	int newHashSize = 25;
	std::list <char>* newHashTable = RehashTable(hashTable, hashSize, newHashSize);

	cout << endl;
	// Traverse rehashed table
	cout << "Traversal of rehashed table: " << endl;
	TraverseTable(newHashTable, newHashSize);

	delete[] newHashTable;

	return 0;
}