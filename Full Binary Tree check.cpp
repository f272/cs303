#include <iostream>
#include <vector>
#include "BTNode.h"
#include "Binary Tree.h"

using namespace std;

int main() {
    /* Construct the following tree
            1000
           /   \
          /     \
         45      500
        /  \     /
       /    \   /   
      15    25 150      
             
             
                
  */

    // Create left child of left binary subtree 
    BinaryTree BL1(new Node(15));
    // Create right child of left binary subtree
    BinaryTree BL2(new Node(25));

    // Create left binary subtree with a left and right subtrees (children) BL1 and BL2
    BinaryTree BL(45, BL1, BL2);

    // Create left child of right binary subtree
    BinaryTree BR1(new Node(150));

    // Create right binary subtree with a left child
    BinaryTree BR(500, BR1, NULL);


	// Create a binary tree with root node 1000, and BL and BR as left and right subtrees
     BinaryTree B(1000, BL, BR);


    // Run function to check if tree is a full binary tree
    cout << "Is tree a full binary tree? (0= false, 1= true): " << B.fullBTCheck() << endl;
    
	return 0;
}