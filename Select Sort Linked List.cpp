#include <iostream> 
#include <iterator>
#include <list>

using namespace std;


void printList(list<int> list) {
	for (std::list<int>::iterator iter = list.begin();
		iter != list.end(); ++iter) {
		cout << *iter << " ";
		cout << endl;
	}
}

void SelectionSort(list<int> list) {

	for (std::list<int>::iterator iter = list.begin();
		iter != list.end(); ++iter) {
		// Set first element of array equal to the 'minimum value'
			int min = *iter;
			// Create an minIndex iterator and set it equal to first element of list
			std::list<int>::iterator minIndex = iter;
	

			// Compare values starting with the number after the index of the minimum value
			for (std::list<int>::iterator iter2 = next(iter, 1);
				iter2 != list.end(); ++iter2) {

			if (*iter2 < min) {
				min = *iter2;
				// Move index of minimum value
				minIndex = iter2;
			}
		}
		// After executing inner loop, if index of minimum value has changed, swap value at index i with the value at the updated minimum index
		if (minIndex != iter) {
			cout << "Yes, minimum index has changed." << endl;
			// Store the value of the iterator at index in a temp variable
			int temp = *iter;
			cout << temp << " is now " << *minIndex << endl;
			*iter= *minIndex;
			*minIndex = temp;
		}

	}

	cout << "After sorting: " << endl;
	printList(list);

}

	int main()
	{
		// Initialize a linked list
		list<int> list1;
		list1.push_back(9);
		list1.push_back(5);
		list1.push_back(3);
		list1.push_back(1);
		list1.push_back(2);
		list1.push_back(7);
		list1.push_back(8);
		
		cout << "Before sorting: " << endl;
		printList(list1);

		SelectionSort(list1);
		
		return 0;
	}