#include <iostream>
using namespace std;

// Declare counter variable
int selectCounter;
int bubbleCounter;
int shellCounter;
int quickCounter;
int mergeCounter;


void randArray(int* arr) {

    for (int i = 0; i < 1000; i++) {
        arr[i] = rand();
    }
}

void print(int* arr, int size) {
    for (int i = 0; i < size; i++)
        cout << arr[i] << " ";
    cout << endl;
}


void sortBubble(int* arr, int size) {
    bubbleCounter = 0;
	bool flag;
    // Initialize counter
	for (int i = 0; i < (size - 1); i++) { // size - 1 for last value because we can only compare next to last element with last element
            for (int j = 0; j < (size - i - 1); j++)  // After each iteration, you subtract 1 from the last element as it should be in the first place
			if (arr[j] > arr[j + 1]) { // flag is only true if we don't need to perform any swapping operations 
                int temp = arr[j];
				arr[j] = arr[j + 1];
				arr[j + 1] = temp;
                // Increment counter by 1 due to swap
                bubbleCounter++;
				flag = false;
			}
	if (flag) // If flag is true, break out of for loop
		break;
	}

    cout << "Total number of element changes: " << bubbleCounter << endl;
}


void sortShell(int* arr, int n) {
    // Initialize counter
    shellCounter = 0;
    for (int gap = n / 2; gap > 0; gap /= 2) {
        // Process subarray using insertion sort
        for (int i = gap; i < n; i += 1) {
            int temp = arr[i];
            int j;
            for (j = i; j >= gap && arr[j - gap] > temp; j -= gap) {
                arr[j] = arr[j - gap];
                shellCounter++;
            }
            arr[j] = temp;
        }
        //
    }

    cout << "Total number of element changes: " << shellCounter << endl;
}

int partition(int arr[], int low, int high) {
    int pivot = arr[high]; // pivot
    int i = (low - 1); // Index of smaller element

    for (int j = low; j <= high - 1; j++)
    {
        // If current element is smaller than the pivot
        if (arr[j] < pivot)
        {
            quickCounter++;
            i++; // increment index of smaller element
            swap(arr[i], arr[j]);
            /*cout << "Current value of quick counter: " << quickCounter << endl;*/
        }
    }
    quickCounter++;
    swap(arr[i + 1], arr[high]);
    return (i + 1);
}

void quickSort(int arr[], int low, int high) {
    if (low < high) {
        // Use partition method to find pivot index 
        int pi = partition(arr, low, high);
        
        // Partition function call insures pivot is in right spot. Call quick sort on both sides of pivot index. 
        quickSort(arr, low, pi - 1);
        quickSort(arr, pi + 1, high);
    }
}

void merge(int arr[], int l, int m, int r) {
    int i, j, k;
    int n1 = m - l + 1;
    int n2 = r - m;

    /* create temp arrays; should be dynamic arrays since size of regular array can't be defined as a variable */
    int* L = new int[n1];
    int* R = new int[n2];
    // If you create arrays of pointers make sure to delete them to free up memory after function is done

    /* Copy data to temp arrays L[] and R[] */
    for (i = 0; i < n1; i++)
        L[i] = arr[l + i];
    for (j = 0; j < n2; j++)
        R[j] = arr[m + 1 + j];

    /* Merge the temp arrays back into arr[l..r]*/
    i = 0; // Initial index of first subarray
    j = 0; // Initial index of second subarray
    k = l; // Initial index of merged subarray
    while (i < n1 && j < n2)
    {
        mergeCounter++;
        // The smaller value (L in this case) will be moved to the bigger array arr
        if (L[i] <= R[j])
        {
            arr[k] = L[i];
            // Increment counter on array containing smaller value
            i++;
        }
        // The smaller value (R in this case) will be moved to the bigger array arr
        else
        {
            arr[k] = R[j];
            // Increment counter on array containing smaller value
            j++;

        }
        // Increment counter on larger array as it fills up
        k++;
    }

    /* Copy the remaining elements of L[], if there
     are any */
    while (i < n1)
    {
        mergeCounter++;
        arr[k] = L[i];
        i++;
        k++;
    }

    /* Copy the remaining elements of R[], if there
     are any */
    while (j < n2)
    {
        mergeCounter++;
        arr[k] = R[j];
        j++;
        k++;
    }
    // Deallocate memory taken by L and R
    delete[] L;
    delete[] R;
}

void mergeSort(int arr[], int l, int r) { //int l, int r = indices of 1st and last element in array
    if (l < r) { // Base case
        // Calculate index of element in middle
        int m = l + (r - l) / 2;

        // Recursively call merge sort on first half of data up to element in middle
        mergeSort(arr, l, m);
        // Recursively call merge sort on second half of data after middle index
        mergeSort(arr, m + 1, r);

        // Call merge function to merge 2 sorted subarrays into one array
        merge(arr, l, m, r);
        //  << endl << "current value of counter: " << counter << endl;
    }
}

void sortSelection(int* arr, int size) {
    selectCounter = 0;
    for (int i = 0; i < size - 1; i++) {
        // Set first element of array equal to the 'minimum value'
        int min = arr[i];
        int minIndex = i;
        // Compare values starting with the number after the index of the minimum value
        for (int j = i + 1; j < size; j++) {
            if (arr[j] < min) {
                min = arr[j];
                minIndex = j;
            }
        }
        // After executing inner loop, if index of minimum value has changed, swap value at index i with the value at the updated minimum index
        if (minIndex != i) {
            int temp = arr[i];
            arr[i] = arr[minIndex];
            arr[minIndex] = temp;
            selectCounter++;
        }
      
    }

   /* cout << "Selection sorted array: " << endl;
    print(arr, size);*/

    cout << "Total number of element changes: " << selectCounter << endl;
}

int main() {
    int bubbleArr1[1000];
    randArray(bubbleArr1);
    cout << "1st array counter (Bubble Sort): " << endl;
    sortBubble(bubbleArr1, 1000);

    int bubbleArr2[1000];
    randArray(bubbleArr2);
    cout << "2nd array counter (Bubble Sort): " << endl;
    sortBubble(bubbleArr2, 1000);

    int bubbleArr3[1000];
    randArray(bubbleArr3);
    cout << "3rd array counter (Bubble Sort): " << endl;
    sortBubble(bubbleArr3, 1000);
    cout << endl;

    int shellArr1[1000];
    randArray(shellArr1);
    cout << "1st array counter (Shell Sort): " << endl;
    sortShell(shellArr1, 1000);

    int shellArr2[1000];
    randArray(shellArr2);
    cout << "2nd array counter (Shell Sort): " << endl;
    sortShell(shellArr2, 1000);

    int shellArr3[1000];
    randArray(shellArr3);
    cout << "3rd array counter (Shell Sort): " << endl;
    sortShell(shellArr3, 1000);
    cout << endl;


    int quickArr1[1000];
    randArray(quickArr1);
    cout << "1st array counter (Quick Sort): " << endl;
    quickSort(quickArr1, 0, 999);
    cout << quickCounter << endl;

    int quickArr2[1000];
    // Clear value of quickCounter before running quickSort again
    quickCounter = 0;
    randArray(quickArr2);
    cout << "2nd array counter (Quick Sort): " << endl;
    quickSort(quickArr2, 0, 999);
    cout << quickCounter << endl;

    int quickArr3[1000];
    // Clear value of quickCounter before running quickSort again
    quickCounter = 0;
    randArray(quickArr3);
    cout << "3rd array counter (Quick Sort): " << endl;
    quickSort(quickArr3, 0, 999);
    cout << quickCounter << endl;
    cout << endl;

    int mergeArr1[1000];
    randArray(mergeArr1);
    cout << "1st array counter (Merge Sort): " << endl;
    mergeSort(mergeArr1, 0, 999);
    cout << mergeCounter << endl;

    int mergeArr2[1000];
    // Clear value of mergeCounter before running mergeSort again
    mergeCounter = 0;
    randArray(mergeArr2);
    cout << "2nd array counter (Merge Sort): " << endl;
    mergeSort(mergeArr2, 0, 999);
    cout << mergeCounter << endl;

    int mergeArr3[1000];
    // Clear value of mergeCounter before running mergeSort again
    mergeCounter = 0;
    randArray(mergeArr3);
    cout << "3rd array counter (Merge Sort): " << endl;
    mergeSort(mergeArr3, 0, 999);
    cout << mergeCounter << endl;
    cout << endl;

    int selectArr1[1000];
    randArray(selectArr1);
    cout << "1st array counter (Selection Sort): " << endl;
    sortSelection(selectArr1, 1000);

    int selectArr2[1000];
    randArray(selectArr2);
    cout << "2nd array counter (Selection Sort): " << endl;
    sortSelection(selectArr2, 1000);

    int selectArr3[1000];
    randArray(selectArr3);
    cout << "3rd array counter (Selection Sort): " << endl;
    sortSelection(selectArr3, 1000);
    cout << endl;
 

    return 0;
}