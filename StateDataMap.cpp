#include <iostream> 
#include <iterator> 
#include <map> 

#include "StateDataMap.h"

using namespace std;

// Method to print contents of stateDataMap
void printMap(map<string, string> data)
{
	map<string, string>::iterator itb = data.begin();
	map<string, string>::iterator ite = data.end();
	for (; itb != ite; itb++) {
		cout << "State: " << itb->first << ", capital: " << itb->second << endl;
	}
}

// Write a C++ method that outputs the capitalName using the stateName 
void printCapital(map<string, string> data) {
	// Create temp string variable to store state name
	string stateName;
	cout << "Please enter the state name: " << endl;
	cin >> stateName;

	// Return iterator pointing to correct key 
	map<string, string>::iterator iter = data.find(stateName);

	// Print capital corresponding to state name
	cout << "The capital of " << stateName << " is " << iter->second << endl;
	cout << endl;
}

int main() {
	map<string, string> m;
	
	// Write C++ statements that add the following pairs to stateDataMap: 
	// (Nebraska, Lincoln), (New York, Albany), (Ohio, Columbus), (California,
	//	Sacramento), (Massachusetts, Boston), and (Texas, Austin).
	m.insert(pair<string, string>("Nebraska", "Lincoln"));
	m.insert(pair<string, string>("New York", "Albany"));
	m.insert(pair<string, string>("Ohio", "Columbus"));
	m.insert(pair<string, string>("California", "Sacramento"));
	m.insert(pair<string, string>("Massachusetts", "Boston"));
	m.insert(pair<string, string>("Texas", "Austin"));

	printMap(m);

	cout << endl;

	// Write a C++ statement that changes the capital of California to Los Angeles
	// Return iterator pointing to correct key (California) first
	map<string, string>::iterator iter1 = m.find("California"); 

	// Change value of capital associated with key (California) to a different city
	iter1->second = "Los Angeles";

	// Print contents of map to show changed capital for state of California
	cout << "Modified state map: " << endl;
	printMap(m);
	cout << endl;

	// Run function to generate capital from state name inputted 
	printCapital(m);

	system("pause");
	return 0;
}