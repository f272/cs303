#pragma once

#ifndef POSTFIX_CONVERTER_H_
#define POSTFIX_CONVERTER_H_

#include <string>
#include <stack>
#include <algorithm>

using namespace std;

class PostfixConverter {
public:
	// Converts infix string expression into a string postfix expression
	string ConvertInfix(string& expression);

	// Evaluates postfix string and returns result as an int
	int EvalPostfix(string& postfixString);

private:
	// Function to determine if character in string is an operator
	bool isOperator(char input) const;

	// Function to determine precedence of an operator
	int precedence(char input);

	// Variables:
	// operator stack
	stack<char> operatorStack;

	// operand stack
	stack<int> operandStack;

	// postfix output string
	string postfixString;

};

#endif