// Main function to test functions within PostfixConverter class 
#include "Postfix converter.h"
#include <iostream>
#include <string>

using namespace std;
int main() {
	// Create postfixConverter object
	PostfixConverter postfixConverter;

	string infixString;
	cout << "Enter infix expression: ";
	getline(cin, infixString);

	string postfixString = postfixConverter.ConvertInfix(infixString);
	cout << "Postfix string: " << postfixString << endl;

	int result = postfixConverter.EvalPostfix(postfixString);
	cout << "Evaluated results: " << result << endl;
		
}