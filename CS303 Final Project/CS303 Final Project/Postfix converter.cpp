#include "Postfix Converter.h"
#include <string>
#include <iostream>
#include <algorithm> // To implement removeif function
#include <stack>
#include <cctype> // Includes functions to check char variables and/or convert them to other datatypes

using namespace std;


string PostfixConverter::ConvertInfix(string& infixString) {
	// Initialize postfix string
	string postfixString = "";

	// Remove spaces, if any, in input infix string
	infixString.erase(remove_if(infixString.begin(), infixString.end(), isspace), infixString.end());

	// Make sure operator stack is empty by popping all elements from it
	while (!operatorStack.empty())
	{
		operatorStack.pop();
	}

	// Use a for loop to push each character of the string into the postfix output stack
	for (int i = 0; i < infixString.length(); i++) {

		// If the character of the string is an operand, push it onto the postfixStack
		if (isalnum(infixString.at(i))) {

		// Create a temp variable to store number, then use a while loop to load all digits of number as a single string to stack
		string temp = "" + infixString.at(i);

			// If we are not at the beginning of the string of numbers being read into the postfix string, and the value before is not a number, add a space to the postfix string
			if (i != 0 && !isalnum(infixString.at(i - 1)))
			{
				postfixString += " ";
			}
			postfixString += infixString.at(i);
		}

		// Check if the character in the string is one of the accepted operators
		if (isOperator(infixString.at(i))) {

			// Run function checking precedence of character and store output in temp variable
			int prec = precedence(infixString.at(i));

			// If the precedence of new operator > top operator in operator stack or the operator stack is empty 
			// or the operator is a left parentheses (- Add it directly to the postfix string
			if (operatorStack.empty() || prec > precedence(operatorStack.top()) || infixString.at(i) == '('){
				operatorStack.push(infixString.at(i));
			}

			// If the new operator is a right parentheses ), pop elements until you reach a left parentheses(while adding them to postfix string* /
			else if (infixString.at(i) == ')') {
				while (!operatorStack.empty() &&
					operatorStack.top() != '(') {
					postfixString += " ";
					postfixString += operatorStack.top();
					operatorStack.pop();
				}
				// Pop that left parentheses and don't add right parentheses to postfix string (consider it removed)
				operatorStack.pop();
			}
			// If the precedence of new operator <= top operator in operator stack- Pop elements and add them to postfix string
			// until you reach an operator that is less than the operator being inserted. NEVER add parentheses to postfix string.
			else if (prec < precedence(operatorStack.top()) || prec == precedence(operatorStack.top())) {
				while (!operatorStack.empty() &&
					(precedence(infixString.at(i)) <= precedence(operatorStack.top()))) {
					// Before inserting into postfix string, add a space
					postfixString += " ";
					postfixString += operatorStack.top();
					operatorStack.pop();
				}
				operatorStack.push(infixString.at(i));
			}
		}

	}

	// Pop and print all remaining operators in operator stack
	while (!operatorStack.empty()) {
		postfixString += " ";
		postfixString += operatorStack.top();
		cout << postfixString << endl;
		operatorStack.pop();
	}

	cout << postfixString << endl;
	return postfixString;
}

int PostfixConverter::EvalPostfix(string& postfixString) {
	for (int i = 0; i < postfixString.length(); i++) {

		// Check if character is a space
		if ((postfixString.at(i)) == ' ') {
			// Skip to next iteration without adding any value to operand stack
			continue;
		}


		// Check if character is an operand
		if (isalnum(postfixString.at(i))) {
			// // Create a temp variable to store number, then use a while loop to load all digits of string as a single int to stack
			string temp = "";
			while (isalnum(postfixString.at(i))) {
				temp += postfixString.at(i);
				i++;
			}
			int operand = stoi(temp);

			// Push operand onto operand stack
			operandStack.push(operand);
		}

		// Check if character is an operator
		if (isOperator(postfixString.at(i))) {
			// Store right operand in temp variable
			int rightOperand = operandStack.top();

			// Pop top operand
			operandStack.pop();

			// Store left operand in temp variable
			int leftOperand = operandStack.top();

			// Pop top operand
			operandStack.pop();

			// Create result variable to store result of operation on 2 operands
			int result = 0;

			// Based on operator detected, perform an operation on the left and right operands and store in result variable
			if (postfixString.at(i) == '+') {

				result = leftOperand + rightOperand;

			}

			else if (postfixString.at(i) == '-') {

				result = leftOperand - rightOperand;

			}

			else if (postfixString.at(i) == '*') {

				result = leftOperand * rightOperand;

				}

			else if (postfixString.at(i) == '/') {

				result = leftOperand / rightOperand;

				}

			// Push result into operand stack
			operandStack.push(result);
		}
	}

	// Return result at top of operand stack
	return operandStack.top();

	// Pop last item off stack
	operandStack.pop();

	// If operand stack is not empty by now, output error message
	if (!operandStack.empty()) {
		cout << "Error: Stack not empty!" << endl;
	}

}

bool PostfixConverter::isOperator(char input) const{
	return (input == '+' || input == '-' || input == '*' || input == '/'
		|| input == '(' || input == ')');
}

int PostfixConverter::precedence(char input){
	// Use if statements to set precedence of given operators
	if (input == '+' || input == '-') {
		return 1;
	}
	
	if (input == '*' || input == '/') {
		return 2;
	}

	if (input == '*' || input == '/') {
		return 2;
	}

	if (input == '(' || input == ')') {
		return -1;
	}
}